//
//  ViewController.swift
//  webview
//
//  Created by Benz on 4/10/2561 BE.
//  Copyright © 2561 Benz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
   @IBOutlet weak var webView: UIWebView!
    let URL2 = "example.com"
    let ExpTime = TimeInterval(60 * 60 * 24 * 365)
    func setCookie(key: String, value: AnyObject) {
        let cookieProps: [HTTPCookiePropertyKey : Any] = [
            HTTPCookiePropertyKey.domain: URL2,
            HTTPCookiePropertyKey.path: "/",
            HTTPCookiePropertyKey.name: key,
            HTTPCookiePropertyKey.value: value,
            HTTPCookiePropertyKey.secure: "TRUE",
            HTTPCookiePropertyKey.expires: NSDate(timeIntervalSinceNow: ExpTime)
        ]
        
        if let cookie = HTTPCookie(properties: cookieProps) {
            HTTPCookieStorage.shared.setCookie(cookie)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
//        let urlstring = "https://ggame.fun/games/hostgame/game.php"
//        let url = URL(string: urlstring)!
//        let request = NSMutableURLRequest(url: url as URL)
//        request.httpMethod = "POST"
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        let dictionary :[String:String] = ["username":"admin","username2":"admin2","username3":"admin3","username4":"admin4"]
//        request.httpShouldHandleCookies  = true
//
//
//        request.httpBody = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted)
//        self.webView.delegate = self
//        self.webView.loadRequest(request as URLRequest)
        let idString = "hello";
        
//        var request = URLRequest(url: URL(string: "https://ggame.fun/games/hostgame/game.php")!)
//        request.httpMethod = "POST"
//        let postString = "id=\(idString)"
//        request.httpBody = postString.data(using: .utf8)
//        webView.loadRequest(request)
        
        var request = URLRequest(url: URL(string: "https://ggame.fun/games/hostgame/game.php")!)
        request.httpMethod = "POST"
        
        let postString = "dataone=testpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksadtestpostoksad"
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            if error != nil {
                print("error=\(error)")
                return
            }
            
            print("response = \(response)")
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("responseString = \(responseString)")
        }
         webView.loadRequest(request)
     //   task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

//MARK : - WebView delegate
extension ViewController : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        
        //    webView.request.coo
        
        let headers = webView.request?.allHTTPHeaderFields
        for (key,value) in headers! {
            print("key \(key) value \(value)")
        }
        
        guard let data = webView.request?.httpBody else {
            return
        }
        
        let responseString = String(data: data, encoding: .utf8)
        print("responseString = \(responseString)")
    }
}
