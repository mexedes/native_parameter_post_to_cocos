// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html
'use strict';

import crypto from 'crypto';
import BufferList from 'bl';
import isArray from 'lodash.isarray';
cc.Class({
    extends: cc.Component,

    properties: {
            label : cc.Label ,
            labelde : cc.Label 
    },



    start () {
        console.log('dddxxxd');


	var plainText = 'xxxxx';
	 var iv = this.generateRandomIV(16); 
	 var key = this.getHashSha256('secretkey', 32); 

        var s = this.encrypt(plainText, key, iv);
        this.label.string = s.toString();

        var d = this.decrypt(s, key, iv);
        this.labelde.string = d.toString();
      //  decrypt:function(encryptedText, key, initVector)


    // console.log(s) ;
    },

    ctor:function() {
        this._maxKeySize = 32;
        this._maxIVSize = 16;
        this._algorithm = 'AES-256-CBC';
        this._characterMatrixForRandomIVStringGeneration = [
          'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
          'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
          'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
          'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
        ];
      },
    
   
      _encryptDecrypt:function(text, key, initVector, isEncrypt) {
    
        if (!text || !key) {
          throw 'cryptLib._encryptDecrypt: -> key and plain or encrypted text '+
           'required';
        }
    
        let ivBl = new BufferList(),
            keyBl = new BufferList(),
            keyCharArray = key.split(''),
            ivCharArray = [],
            encryptor, decryptor, clearText;
    
        if (initVector && initVector.length > 0) {
           ivCharArray = initVector.split('');
        }
        
        for (var i = 0; i < this._maxIVSize; i++) {
          ivBl.append(ivCharArray.shift() || [null]);
        }
    
        for (var i = 0; i < this._maxKeySize; i++) {
          keyBl.append(keyCharArray.shift() || [null]);
        }
    
        if (isEncrypt) {
          encryptor = crypto.createCipheriv(this._algorithm, keyBl.toString(), 
            ivBl.toString());
          encryptor.setEncoding('base64');
          encryptor.write(text);
          encryptor.end();
          return encryptor.read();
        }
    
        decryptor = crypto.createDecipheriv(this._algorithm, keyBl.toString(),
          ivBl.toString());
        var dec = decryptor.update(text, 'base64', 'utf8');
        dec += decryptor.final('utf8');
        return dec;
      },
    
   
      _isCorrectLength:function(length) {
        return length && /^\d+$/.test(length) && parseInt(length, 10) !== 0
      },
    
  
      generateRandomIV:function(length) {
        if (!this._isCorrectLength(length)) {
          throw 'cryptLib.generateRandomIV() -> needs length or in wrong format';
        }
    
        length = parseInt(length, 10);
        let _iv = [],
            randomBytes = crypto.randomBytes(length);
    
        for (let i = 0; i < length; i++) {
          let ptr = randomBytes[i] % 
            this._characterMatrixForRandomIVStringGeneration.length;
          _iv[i] = this._characterMatrixForRandomIVStringGeneration[ptr];
        }
        return _iv.join('');
      },
    

      getHashSha256:function(key, length) {
        if (!key) {
          throw 'cryptLib.getHashSha256() -> needs key';
        }
    
        if (!this._isCorrectLength(length)) {
          throw 'cryptLib.getHashSha256() -> needs length or in wrong format';
        }
    
        return crypto.createHash('sha256')
                     .update(key)
                     .digest('hex')
                     .substring(0, length);
      },
    

      encrypt:function(plainText, key, initVector) {
        return this._encryptDecrypt(plainText, key, initVector, true);
      },
    

      decrypt:function(encryptedText, key, initVector) {
        return this._encryptDecrypt(encryptedText, key, initVector, false);
      }
});
